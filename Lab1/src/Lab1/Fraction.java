package Lab1;

public class Fraction {
    private int _numerator;
    private int _denominator;
    private void Negate(){
        this._numerator = -this._numerator;
    }
    private void Normalize(){
        if (this._numerator == 0){
            return;
        }
        int gcd = Util.Gcd(this._numerator, this._denominator);
        this._numerator /= gcd;
        this._denominator /= gcd;
    }
    public Fraction(int numerator, int denominator){
        if (denominator == 0){
            throw new ArithmeticException("Zero in denominator");
        }
        this._numerator = numerator;
        this._denominator = denominator;
        this.Normalize();
    }
    public Fraction(){
        this._numerator = 1;
        this._denominator = 1;
    }
    public Fraction Add(Fraction a){
        Fraction tmp = new Fraction();
        int gcd = Util.Gcd(this._denominator, a._denominator);
        int left = this._denominator / gcd;
        int right = a._denominator / gcd;
        tmp._denominator = left * gcd * right;
        tmp._numerator = right * this._numerator + left * a._numerator;
        tmp.Normalize();
        return tmp;
    }
    @Override
    public String toString(){
        return Integer.toString(this._numerator) + '/' + this._denominator;
    }
}
