package Lab1;

public class Calculator {
    public static Fraction findSum(int n){
        Fraction sum = new Fraction(0, 1);
        for (int i = 0; i < n; i++) {
            sum = sum.Add(new Fraction(1, i + 1));
        }
        return sum;
    }
}
