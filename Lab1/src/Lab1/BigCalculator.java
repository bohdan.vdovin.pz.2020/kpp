package Lab1;

import java.math.BigInteger;

public class BigCalculator {
    public static BigFraction findSum(int n){
        BigFraction sum = new BigFraction(BigInteger.valueOf(0),BigInteger.valueOf(1));
        for (int i = 0; i < n; i++) {
            sum = sum.add(new BigFraction(BigInteger.valueOf(1), BigInteger.valueOf(i + 1)));
        }
        return sum;
    }
}
