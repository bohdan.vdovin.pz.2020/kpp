package Lab1;

import java.math.BigInteger;
import java.util.Objects;

public class BigFraction {
    private BigInteger _numerator;
    private BigInteger _denominator;
    private void Negate(){
        this._numerator = this._numerator.negate();
    }
    private void normalize(){
        if (Objects.equals(this._numerator, BigInteger.valueOf(0))){
            return;
        }
        BigInteger gcd = this._numerator.gcd(this._denominator);
        this._numerator = this._numerator.divide(gcd);
        this._denominator = this._denominator.divide(gcd);
    }
    public BigFraction(BigInteger numerator, BigInteger denominator){
        if (Objects.equals(denominator, BigInteger.valueOf(0))){
            throw new ArithmeticException("Zero in denominator");
        }
        this._numerator = numerator;
        this._denominator = denominator;
        this.normalize();
    }
    public BigFraction(){
        this._numerator = BigInteger.valueOf(1);
        this._denominator = BigInteger.valueOf(1);
    }
    public BigFraction add(BigFraction a){
        BigFraction tmp = new BigFraction();
        BigInteger gcd = this._denominator.gcd(a._denominator);
        BigInteger left = this._denominator.divide(gcd);
        BigInteger right = a._denominator.divide(gcd);
        tmp._denominator = left.multiply(gcd).multiply(right);
        tmp._numerator = right.multiply(this._numerator).add(left.multiply(a._numerator));
        tmp.normalize();
        return tmp;
    }
    @Override
    public String toString(){
        return this._numerator.toString() + '/' + this._denominator.toString();
    }
}
