package Lab1;
public class Util {
    public static int Gcd(int a, int b){
        if (a > b){
            Gcd(b, a);
        }
        int mod = b % a;
        if (mod == 0){
            return a;
        } else{
            return Gcd(mod, a);
        }
    }
}
