import Lab1.BigCalculator;
import Lab1.BigFraction;
import Lab1.Calculator;
import Lab1.Fraction;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        if( n > 0 && n < 15) {
            Fraction sum = Calculator.findSum(n);
            System.out.println(sum);
        } else if (n >= 16) {
            BigFraction sum = BigCalculator.findSum(n);
            System.out.println(sum);
        }
    }
}