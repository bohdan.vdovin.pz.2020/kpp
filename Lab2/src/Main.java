import Lab2.*;

import java.util.*;

public class Main {
    public static Collection<PieceOfSportClothing> genPieceOfClothing(int n){
        List<HashMap<Material, Float>> aMaterials = new ArrayList<HashMap<Material, Float>>();
        float step = 1f / n;
        for(int i=0; i < 9; i++) {
            HashMap<Material, Float> materials = new HashMap<Material, Float>();
            materials.put(new Material("cotton"), step * i);
            materials.put(new Material("polyester"), 1f - step*i);
            aMaterials.add(materials);
        }
        {
            HashMap<Material, Float> materials = new HashMap<Material, Float>();
            materials.put(new Material("polyester"), 1.0f);
            aMaterials.add(materials);
        }
        List<PieceOfSportClothing> res = new ArrayList<PieceOfSportClothing>();
        res.add(new Sneaker("Super sneaker", new Brand("Adidas"), 102.0f, aMaterials.get(0).entrySet(), 25f));
        res.add(new Sneaker("SS", new Brand("Nike"), 106.0f, aMaterials.get(1).entrySet(), 27f));
        res.add(new Sneaker("Teo", new Brand("Puma"), 120.0f, aMaterials.get(2).entrySet(), 23f));
        res.add(new Pant("Rome", new Brand("Nike"), 102.0f, aMaterials.get(3).entrySet(), 10));
        res.add(new Pant("London", new Brand("Puma"), 94.0f, aMaterials.get(4).entrySet(), 11));
        res.add(new Pant("New", new Brand("Nike"), 106.0f, aMaterials.get(5).entrySet(), 11));
        res.add(new Socks("Energic", new Brand("Adidas"), 32.0f, aMaterials.get(6).entrySet(), Socks.SocksType.EXTRA_LOW_CUT));
        res.add(new Socks("Timer", new Brand("Adidas"), 27.0f, aMaterials.get(7).entrySet(), Socks.SocksType.LOOSE_SOCKS));
        res.add(new Socks("Teleporter", new Brand("Nike"), 36.0f, aMaterials.get(8).entrySet(), Socks.SocksType.KNEE));
        res.add(new Socks("Torozer", new Brand("Nike"), 36.0f, aMaterials.get(9).entrySet(), Socks.SocksType.KNEE));
        return res;
    }

    public static void printArray(Collection<PieceOfSportClothing> clothing){
        for(PieceOfSportClothing cloth: clothing){
            System.out.printf("> %s%n", cloth.toString());
        }
    }

    public static void main(String[] args) {
        // Setup
        PieceOfSportClothingManager mgr = new PieceOfSportClothingManager(genPieceOfClothing(10));

        // Search
        System.out.println("- Search 1. By brand");
        printArray(mgr.findByBrand(new Brand("Adidas")));
        System.out.println("\n");

        System.out.println("- Search 2. By price");
        printArray(mgr.findByPriceRange(100f, 200f));
        System.out.println("\n");

        // Sort
        System.out.println("- Sort 1. Using cotton content comparator: static inner class, ascending order");
        printArray(mgr.sortedByCottonContent(PieceOfSportClothingManager.SortOrder.ASCENDING));
        System.out.println("\n");

        System.out.println("- Sort 2. Using price comparator: inner class, ascending order");
        printArray(mgr.sortedByPrice(PieceOfSportClothingManager.SortOrder.ASCENDING));
        System.out.println("\n");

        System.out.println("- Sort 3. Using name comparator: anonymous class, descending order");
        printArray(mgr.sortedByName(PieceOfSportClothingManager.SortOrder.DESCENDING));
        System.out.println("\n");

        System.out.println("- Sort 4. Using brand name comparator: lambda, descending order");
        printArray(mgr.sortedByBrandName(PieceOfSportClothingManager.SortOrder.DESCENDING));

    }
}