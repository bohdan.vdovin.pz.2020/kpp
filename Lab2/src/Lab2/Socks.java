package Lab2;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

public class Socks extends PieceOfSportClothing {
    public enum SocksType{
        MID_CALF,
        OVER_THE_CALF,
        KNEE,
        LOOSE_SOCKS,
        OVER_THE_KNEE,
        TOE_COVER,
        INVISIBLE,
        EXTRA_LOW_CUT,
        QUARTER

    };
    private SocksType _socksType;
    public SocksType getSockType() {return this._socksType;}
    public Socks(String name, Brand brand, Float price, Collection<Map.Entry<Material, Float>> materials, SocksType socksType){
        super(name, brand, price, materials);
        this._socksType = socksType;
    }

    @Override
    public String wear(){
        String tmp = String.format("Socks. It's got to have warm socks %s. Now it's time to exercise!", this.getName());
        return tmp;
    }

    @Override
    public String toString(){
        StringBuilder tmp = new StringBuilder(String.format("Socks. Name %s. Brand %s. Price %f.", this.getName(), this.getBrand(), this.getPrice()));
        Collection<Map.Entry<Material, Float>> materials = this.getMaterials();
        tmp.append(" Materials: ");
        for(Map.Entry<Material, Float> material: materials){
            tmp.append(material.toString()).append(" ");
        }
        return tmp.toString();
    }
}
