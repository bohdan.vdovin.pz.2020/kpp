package Lab2;

public class Brand implements Comparable<Brand> {
    private String _name;

    public String getName(){return _name;}
    public void setName(String name){ _name = name;}

    public Brand(String name){
        _name = name;
    }

    @Override
    public int compareTo(Brand o) {
        return _name.compareTo(o._name);
    }

    @Override
    public String toString(){
        return _name;
    }
}
