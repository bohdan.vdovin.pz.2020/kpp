package Lab2;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public abstract class PieceOfSportClothing {
    private HashMap<Material, Float> _materials;
    private String _name;
    private Brand _brand;
    private Float _price;

    public Float getPrice() {return _price;}
    public Brand getBrand() {return _brand;}
    public String getName() {return _name;}
    public Collection<Map.Entry<Material, Float>> getMaterials() {return this._materials.entrySet();}
    public Float getMaterialContent(Material material) { return this._materials.getOrDefault(material, 0f);}

    public void setPrice(Float price) {_price = price;}
    public void setBrand(Brand brand) {_brand = brand;}
    public void setName(String name) {_name = name;}


    public PieceOfSportClothing(String name, Brand brand, Float price, Collection<Map.Entry<Material, Float>> materialsPercentage){
        this._price = price;
        this._name = name;
        this._brand = brand;
        this._materials = new HashMap<Material, Float>();
        Float total_percentage = 0.0f;
        for(Map.Entry<Material, Float> val: materialsPercentage){
            this._materials.put(val.getKey(), val.getValue());
            total_percentage += val.getValue();
        }
        if( Math.abs(total_percentage - 1.0f) > 1E-10){
            throw new IllegalArgumentException("Total percentage is not equal to 1");
        }
    }

    public abstract String wear();
}
