package Lab2;

import java.util.Collection;
import java.util.Map;
import java.util.Iterator;

public class Pant extends PieceOfSportClothing {
    private Integer _usaPantSize;
    public Integer getUSAPantSize() {return this._usaPantSize;}
    public Pant(String name, Brand brand, Float price, Collection<Map.Entry<Material, Float>> materials, Integer usaPantSize){
        super(name, brand, price, materials);
        this._usaPantSize = usaPantSize;
    }

    @Override
    public String wear(){
        String tmp = String.format("Sporting pants. It's got to have a pair %s pant. Now it's time to exercise!", this.getName());
        return tmp;
    }

    @Override
    public String toString(){
        StringBuilder tmp = new StringBuilder(String.format("Pant. Name: %s. Brand: %s. Price: %f.", this.getName(), this.getBrand(), this.getPrice()));
        Collection<Map.Entry<Material, Float>> materials = this.getMaterials();
        tmp.append(" Materials: ");
        for(Map.Entry<Material, Float> material: materials){
            tmp.append(material.toString()).append(" ");
        }
        return tmp.toString();
    }
}
