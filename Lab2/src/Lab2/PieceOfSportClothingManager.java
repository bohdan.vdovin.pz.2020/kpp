package Lab2;

import java.util.*;

public class PieceOfSportClothingManager {
    private List<PieceOfSportClothing> _clothing;
    public PieceOfSportClothingManager(Collection<PieceOfSportClothing> clothing){
        this._clothing = new ArrayList<PieceOfSportClothing>();
        this._clothing.addAll(clothing);
    }
    public Collection<PieceOfSportClothing> findByBrand(Brand brand) {
        ArrayList<PieceOfSportClothing> tmp = new ArrayList<>();
        for (PieceOfSportClothing pieceOfSportClothing : this._clothing) {
            if (pieceOfSportClothing.getBrand().compareTo(brand) == 0){
                tmp.add(pieceOfSportClothing);
            }
        }
        return tmp;
    }
    public Collection<PieceOfSportClothing> findByPriceRange(Float start, Float end){
        ArrayList<PieceOfSportClothing> tmp = new ArrayList<>();
        for (PieceOfSportClothing pieceOfSportClothing : this._clothing) {
            if (start <= pieceOfSportClothing.getPrice() && pieceOfSportClothing.getPrice() <= end) {
                tmp.add(pieceOfSportClothing);
            }
        }
        return tmp;
    }

    public Collection<PieceOfSportClothing> sorted(Comparator<PieceOfSportClothing> cmp, SortOrder flag){
        List<PieceOfSportClothing> tmp = new ArrayList<>(this._clothing.stream().toList());
        if (flag == SortOrder.DESCENDING)
            cmp = cmp.reversed();
        tmp.sort(cmp);
        return tmp;
    }

    public Collection<PieceOfSportClothing> sortedByCottonContent(SortOrder flag){
        return this.sorted(new PieceOfSportClothingCCottonContent(), flag);
    }

    public Collection<PieceOfSportClothing> sortedByPrice(SortOrder flag){
        return this.sorted(new PieceOfSportClothingCPrice(), flag);
    }

    public Collection<PieceOfSportClothing> sortedByName(SortOrder flag){
        Comparator<PieceOfSportClothing> cmp = new Comparator<PieceOfSportClothing>() {
            @Override
            public int compare(PieceOfSportClothing o1, PieceOfSportClothing o2) {
                return o1.getName().compareTo(o2.getName());
            }
        };
        return this.sorted(cmp, flag);
    }

    public Collection<PieceOfSportClothing> sortedByBrandName(SortOrder flag){
        Comparator<PieceOfSportClothing> cmp = (PieceOfSportClothing a, PieceOfSportClothing b) -> {return a.getBrand().compareTo(b.getBrand());};
        return this.sorted(cmp, flag);
    }
    public static class PieceOfSportClothingCCottonContent implements Comparator<PieceOfSportClothing> {
        @Override
        public int compare(PieceOfSportClothing a, PieceOfSportClothing b){
            Float a_content = a.getMaterialContent(new Material("cotton"));
            Float b_content = b.getMaterialContent(new Material("cotton"));
            return Float.compare(a_content,b_content);
        }
    }

    public class PieceOfSportClothingCPrice implements Comparator<PieceOfSportClothing> {
        @Override
        public int compare(PieceOfSportClothing a, PieceOfSportClothing b){
            Float a_price = a.getPrice();
            Float b_price = b.getPrice();
            return Float.compare(a_price, b_price);
        }
    }

    public enum SortOrder{
        DESCENDING,
        ASCENDING,
    }
}

