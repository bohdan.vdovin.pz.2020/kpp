package Lab2;

public class Material implements Comparable<Material> {
    private String _name;

    public String getName(){return _name;}
    public void setName(String name){ _name = name;}

    public Material(String name){
        _name = name;
    }

    @Override
    public int compareTo(Material o) {
        return _name.compareTo(o._name);
    }
    @Override
    public String toString(){
        return _name;
    }

    @Override
    public boolean equals(Object other){
        return _name.equalsIgnoreCase(((Material)other).getName());
    }

    @Override
    public int hashCode(){
        return _name.hashCode();
    }

}
