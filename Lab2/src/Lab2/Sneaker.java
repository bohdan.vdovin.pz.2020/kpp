package Lab2;

import java.util.Collection;
import java.util.Map;
import java.util.Iterator;

public class Sneaker extends PieceOfSportClothing {
    private Float _lacesLength;
    public Float getLacesLength() { return this._lacesLength;}
    public Sneaker(String name, Brand brand, Float price, Collection<Map.Entry<Material, Float>> materials, Float lacesLength){
        super(name, brand, price, materials);
        this._lacesLength = lacesLength;
    }

    @Override
    public String wear(){
        String tmp = String.format("Sneakers. It's got to have sneakers %s . Now it's time to run!", this.getName());
        return tmp;
    }

    @Override
    public String toString(){
        StringBuilder tmp = new StringBuilder(String.format("Sneaker. Name %s. Brand %s. Price %f.", this.getName(), this.getBrand(), this.getPrice()));
        Collection<Map.Entry<Material, Float>> materials = this.getMaterials();
        tmp.append(" Materials: ");
        for(Map.Entry<Material, Float> material: materials){
            tmp.append(material.toString()).append(" ");
        }
        return tmp.toString();
    }
}
