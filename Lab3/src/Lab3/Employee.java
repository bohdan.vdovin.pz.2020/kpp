package Lab3;

import java.util.Collection;
import java.util.Comparator;

public class Employee {
    private String _surname;
    private String _role;
    private Integer _birthdayYear;
    private Float _salary;

    public String getSurname() {return _surname;}
    public String getRole() {return _role;}
    public Integer getBirthdayYear() {return _birthdayYear;}
    public Float getSalary() {return _salary;}

    public void setSurname(String name) {
        _surname = name;}
    public void setRole(String role) {_role = role;}
    public void setBirthdayYear(Integer birthdayYear) { _birthdayYear = birthdayYear;}
    public void setSalary(Float salary) { _salary = salary;}

    public Employee(String surname, String role, Integer birthdayYear, Float salary ){
        _surname = surname;
        _role = role;
        _birthdayYear = birthdayYear;
        _salary = salary;
    }
    public boolean equals(Employee othr){
        return this.getSurname().equals(othr.getSurname());
    }

    @Override
    public String toString(){
        return String.format("%s, %s, %d, %f", _surname, _role, _birthdayYear, _salary);
    }
    @Override
    public int hashCode(){
        return this.getSurname().hashCode();
    }
    public SALARY getSalaryCategory(){
        if (_salary > 0 && _salary < 50000f){
            return SALARY.LOW;
        } else if (_salary < 100000f) {
            return SALARY.MID;
        } else {
            return SALARY.HIGH;
        }
    }
    public enum SALARY{
        LOW,
        MID,
        HIGH
    }
}
