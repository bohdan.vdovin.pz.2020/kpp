package Lab3;

import java.util.*;

public class EmployeeManager {
    public static Employee findYoungest(Collection<Employee> employees){
        Employee youngest = employees.stream().toList().get(0);
        for(Employee emp : employees){
            youngest = new EmployeeCAge().compare(emp, youngest) < 0 ? youngest : emp;
        }
        return youngest;
    }

    public static Employee findOldest(Collection<Employee> employees) {
        Employee oldest = employees.stream().toList().get(0);
        for (Employee emp : employees) {
            oldest = new EmployeeCAge().compare(emp, oldest) > 0 ? oldest : emp;
        }
        return oldest;
    };

    public static Employee findRichest(Collection<Employee> employees){
        Employee richest = employees.stream().toList().get(0);
        Employee poorer = employees.stream().toList().get(0);
        for(Employee emp : employees){
            richest = new EmployeeCSalary().compare(emp, richest) < 0 ? richest : emp;
        }
        return richest;
    }

    public static Employee findPoorest(Collection<Employee> employees){
        Employee poorer = employees.stream().toList().get(0);
        for(Employee emp : employees){
            poorer = new EmployeeCSalary().compare(emp, poorer) > 0 ? poorer : emp;
        }
        return poorer;
    }
    public static HashMap<Employee.SALARY, List<Employee>> categorizeBySalary(Collection<Employee> employees){
        HashMap<Employee.SALARY, List<Employee>> catagoriesMapper = new HashMap<Employee.SALARY, List<Employee>>();
        catagoriesMapper.put(Employee.SALARY.LOW, new ArrayList<Employee>());
        catagoriesMapper.put(Employee.SALARY.MID, new ArrayList<Employee>());
        catagoriesMapper.put(Employee.SALARY.HIGH, new ArrayList<Employee>());
        for(Employee emp: employees){
            catagoriesMapper.get(emp.getSalaryCategory()).add(emp);
        }
        return catagoriesMapper;
    }

    public static Collection<Employee> uniqueEmployees(Collection<Employee> a, Collection<Employee> b){
        Set<Employee> uniqueSurnames = new HashSet<Employee>();
        uniqueSurnames.addAll(a);
        uniqueSurnames.addAll(b);
        return uniqueSurnames;
    }
    public static void removeIfBirthdayYearLowerThan(Collection<Employee> employees, Integer n){
        employees.removeIf(emp -> Integer.compare(emp.getBirthdayYear(), n) < 0);
    }

    public static class EmployeeCAge implements Comparator<Employee>{
        @Override
        public int compare(Employee a, Employee b){
            return Integer.compare(a.getBirthdayYear(), b.getBirthdayYear());
        }
    }
    public static  class EmployeeCSalary implements  Comparator<Employee>{
        @Override
        public int compare(Employee a, Employee b){
            return Float.compare(a.getSalary(), b.getSalary());
        }
    }
    public static  class EmployeeCSurname implements  Comparator<Employee>{
        @Override
        public int compare(Employee a, Employee b){
            return Float.compare(a.getSalary(), b.getSalary());
        }
    }
}
