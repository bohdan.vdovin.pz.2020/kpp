package Lab3;

import java.util.Collection;
import java.util.List;

public class Task3 {
    public static void run(List<Employee> list1, List<Employee> list2, Integer n){
        Collection<Employee> uniqueSurnames = EmployeeManager.uniqueEmployees(list1, list2);
        EmployeeManager.removeIfBirthdayYearLowerThan(uniqueSurnames, n);

        System.out.printf("Unique born later than %d%n", n);
        for(Employee emp: uniqueSurnames){
            System.out.println("> " + emp.toString());
        }
    }
}
