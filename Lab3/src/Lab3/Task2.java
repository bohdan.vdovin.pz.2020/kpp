package Lab3;

import java.util.HashMap;
import java.util.List;

public class Task2 {
    public static void run(List<Employee> employees){
        System.out.printf("The employee with highest salary in the list: %s%n", EmployeeManager.findRichest(employees));
        System.out.printf("The employee with lower salary in the list: %s%n\n", EmployeeManager.findPoorest(employees));

        HashMap<Employee.SALARY, List<Employee>> catagoriesMapper = EmployeeManager.categorizeBySalary(employees);
        System.out.println("Employee per category: ");
        System.out.println("- LOW:");
        for(Employee emp: catagoriesMapper.get(Employee.SALARY.LOW)){
            System.out.println("> " + emp.toString());
        }
        System.out.println("- MID:");
        for(Employee emp: catagoriesMapper.get(Employee.SALARY.MID)){
            System.out.println("> " + emp.toString());
        }
        System.out.println("- HIGH:");
        for(Employee emp: catagoriesMapper.get(Employee.SALARY.HIGH)){
            System.out.println("> " + emp.toString());
        }
    }
}
