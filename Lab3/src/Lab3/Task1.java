package Lab3;

import java.util.*;

public class Task1 {
    public static void run(List<Employee> employees){
        HashMap<String, List<Employee>> positionsEmp = new HashMap<>();
        for(Employee employee : employees){
            var tmp = positionsEmp.getOrDefault(employee.getRole(), null);
            if(tmp == null){
                tmp = new ArrayList<>();
                tmp.add(employee);
                positionsEmp.put(employee.getRole(), tmp);
            } else{
                tmp.add(employee);
            }
        }
        for(Map.Entry<String, List<Employee>> roleEmployees: positionsEmp.entrySet()){
            System.out.printf("The youngest employee in the position %s in the list: %s%n", roleEmployees.getKey(), EmployeeManager.findYoungest(roleEmployees.getValue()));
            System.out.printf("The oldest employee in the position %s in the list: %s%n", roleEmployees.getKey(), EmployeeManager.findOldest(roleEmployees.getValue()));
        }
    }
}
