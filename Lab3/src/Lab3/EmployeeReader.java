package Lab3;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class EmployeeReader {
    public static List<Employee> readFromFile(String filename) throws IOException {
        Scanner sc = new Scanner(new File(filename));
        List<Employee> res = new ArrayList<Employee>();
        while (sc.hasNext()){
            String surname = sc.next();
            String role = sc.next();
            Integer birthdayYear = sc.nextInt();
            Float salary = sc.nextFloat();
            res.add(new Employee(surname, role, birthdayYear, salary));
        }
        return res;
    }
}