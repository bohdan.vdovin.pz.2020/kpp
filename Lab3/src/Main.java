import Lab3.*;

import java.io.IOException;
import java.util.*;

public class Main {
    public static void main(String[] args){
        int n;
        System.out.print("Choose option: 1, 2, 3: ");
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();

        if (n == 1){
            System.out.print("Enter the filename: ");
            String filename = sc.next();
            List<Employee> employees = null;
            try {
                employees = EmployeeReader.readFromFile(filename);
            } catch (IOException e) {
                System.out.println("Data exception. File does not follow format.");
                System.exit(1);
            }
            Task1.run(employees);
        } else if (n == 2){
            System.out.print("Enter the filename: ");
            String filename = sc.next();
            List<Employee> employees = null;
            // Find min and max salary and create hash map with key salary category + employees info which belong to it
            try {
                employees = EmployeeReader.readFromFile(filename);
            } catch (IOException e) {
                System.out.println("Data exception. File does not follow format.");
                System.exit(1);
            }
            Task2.run(employees);
        } else if (n == 3){
            System.out.print("Enter the first filename: ");
            String filename1 = sc.next();
            System.out.print("Enter the second filename: ");
            String filename2 = sc.next();
            List<Employee> employees1 = null;
            List<Employee> employees2 = null;
            try {
                employees1 = EmployeeReader.readFromFile(filename1);
                employees2 = EmployeeReader.readFromFile(filename2);
            } catch (IOException e) {
                System.out.println("Data exception. File does not follow format.");
                System.exit(1);
            }
            // Combine two files infos and remove duplicate surnames and remove all workers which age it lover than given
            System.out.print("Enter the year: ");
            int year = sc.nextInt();
            Task3.run(employees1, employees2, year);
        } else {
            System.out.println("There is no such option. Quit");
        }

    }
}